from django.urls import path

from .views import *

urlpatterns =[
    path('', HomeScenarioView.homescenario, name='home_scenario'),
    path('owl_generator/', OwlGeneratorView.owlgenerator, name='owl_generator'),
    path('upload_scenario/', UploadScenarioView.upload_scenario, name='home_scenario'),
    path('fetch_power_types/', fetch_power_types, name='fetch_power_types'),

]